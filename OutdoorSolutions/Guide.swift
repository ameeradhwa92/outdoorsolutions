//
//  Guide.swift
//  OutdoorSolutions
//
//  Created by Ameer on 19/11/2015.
//  Copyright © 2015 Ameer. All rights reserved.
//

import Foundation
import UIKit

class Guide: NSObject, NSCoding{
    var name: String?
    var photo: UIImage?
    var expertise: [Activity]?
    var bio: String?
    var phoneNo: String?
    
    func encodeWithCoder (aCoder: NSCoder){
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.name, forKey: "PLDGuideName")
        keyedArchiver.encodeObject(self.bio, forKey: "PLDGuideBio")
        keyedArchiver.encodeObject(self.phoneNo, forKey: "PLDPhoneNo")
        
        if self.photo != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.photo!)
            keyedArchiver.encodeObject(imageData, forKey: "PLDGuidePhoto")
        }
    }
    
    required convenience init?(coder aDecoder:NSCoder){
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let name: String = keyedUnarchiver.decodeObjectForKey("PLDGuideName") as! String
        let bio = keyedUnarchiver.decodeObjectForKey("PLDGuideBio") as? String
        let phoneNo = keyedUnarchiver.decodeObjectForKey("PLDGuidePhoneNo") as? String
        
        var photo: UIImage? = nil
        
        if let imageData = keyedUnarchiver.decodeObjectForKey("PLDGuidePhoto") as? NSData {
            photo = UIImage (data: imageData)
        }
        
        let expertise: [Activity] = []
        
        self.init(name: name, photo: photo, expertise: expertise, bio: bio, phoneNo: phoneNo)
    }
    
    init(name: String, photo: UIImage?, expertise: [Activity], bio: String?, phoneNo: String?) {
        self.name = name
        self.photo = photo
        self.expertise = expertise
        self.bio = bio
        self.phoneNo = phoneNo
    }
}