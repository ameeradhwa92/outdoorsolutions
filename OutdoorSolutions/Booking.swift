//
//  User.swift
//  OutdoorSolutions
//
//  Created by Ameer on 18/11/2015.
//  Copyright © 2015 Ameer. All rights reserved.
//

import Foundation

class Booking: NSObject, NSCoding {
    var userName: String?
    var userPhoneNo: String?
    var pax: Int?
    var guide: Guide?
    var categories: [Activity]?
    var startDateTime: NSDate?
    var endDateTime: NSDate?
    var confirmationCode: String?
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.userName, forKey: "PLDBookingName")
        keyedArchiver.encodeObject(self.userPhoneNo, forKey: "PLDBookingPhoneNo")
        keyedArchiver.encodeObject(self.pax, forKey: "PLDBookingNoOfPax")
        keyedArchiver.encodeObject(self.guide, forKey: "PLDBookingGuide")
        keyedArchiver.encodeObject(self.categories, forKey: "PLDBookingCategory")
        keyedArchiver.encodeObject(self.startDateTime, forKey: "PLDBookingStartDateTime")
        keyedArchiver.encodeObject(self.endDateTime, forKey: "PLDBookingEndTime")
        keyedArchiver.encodeObject(self.confirmationCode, forKey: "PLDBookingConfirmationCode")
    }
    
    required convenience init?(coder aDecoder:NSCoder){
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let userName: String = keyedUnarchiver.decodeObjectForKey("PLDBookingName") as! String
        
        let userPhoneNo = keyedUnarchiver.decodeObjectForKey("PLDBookingPhoneNo") as? String
        let pax = keyedUnarchiver.decodeObjectForKey("PLDBookingNoOfPax") as? Int
        let guide = (keyedUnarchiver.decodeObjectForKey("PLDBookingGuide") as? Guide?)!
        let startDateTime = (keyedUnarchiver.decodeObjectForKey("PLDBookingStartDateTime") as? NSDate?)!
        let endDateTime = (keyedUnarchiver.decodeObjectForKey("PLDBookingEndDateTime") as? NSDate?)!
        let confirmationCode = keyedUnarchiver.decodeObjectForKey("PLDBookingConfirmationCode") as? String
        
        let categories: [Activity]? = []
        
        self.init(userName: userName,userPhoneNo: userPhoneNo, pax: pax!, guide: guide!, categories: categories!, startDateTime: startDateTime!, endDateTime: endDateTime!, confirmationCode: confirmationCode!)
    }

    
    init(userName: String, userPhoneNo: String?, pax: Int, guide: Guide, categories: [Activity], startDateTime: NSDate, endDateTime: NSDate, confirmationCode: String) {
        self.userName = userName
        self.userPhoneNo = userPhoneNo
        self.pax = pax
        self.guide = guide
        self.categories = categories
        self.startDateTime = startDateTime
        self.endDateTime = endDateTime
        self.confirmationCode = confirmationCode
    }
}