//
//  Company.swift
//  OutdoorSolutions
//
//  Created by Ameer on 19/11/2015.
//  Copyright © 2015 Ameer. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Company: NSObject, NSCoding {
    var name: String?
    var logo: UIImage?
    var address: String?
    var coordinate: CLLocationCoordinate2D?
    var phoneNo: String?
    var guides: [Guide]?
    var bookings: [Booking] = []
    
    func encodeWithCoder (aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.name, forKey: "PLDCompanyName")
        keyedArchiver.encodeObject(self.address, forKey: "PLDCompanyAddress")
        keyedArchiver.encodeObject(self.phoneNo, forKey: "PLDCompanyPhoneNumber")
        keyedArchiver.encodeObject(self.coordinate?.latitude, forKey: "PLDLatitude")
        keyedArchiver.encodeObject(self.coordinate?.longitude, forKey: "PLDLongitude")
        
        if self.logo != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.logo!)
            keyedArchiver.encodeObject(imageData, forKey: "PLDCompanyLogoData")
        }
    }
    
    required convenience init?(coder aDecoder:NSCoder){
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        let name: String = keyedUnarchiver.decodeObjectForKey("PLDCompanyName") as! String
        let address = keyedUnarchiver.decodeObjectForKey("PLDCompanyAddress") as? String
        let latitude = keyedUnarchiver.decodeObjectForKey("PLDLatitude") as! CLLocationDegrees
        let longitude = keyedUnarchiver.decodeObjectForKey("PLDLongitude") as! CLLocationDegrees
        let phoneNo = keyedUnarchiver.decodeObjectForKey("PLDCompanyPhoneNumber") as? String
        
        var logo: UIImage?
        if let imageData = keyedUnarchiver.decodeObjectForKey("PLDCompanyLogoData") as? NSData {
            logo = UIImage(data: imageData)
        }
        
        let guides: [Guide]? = []
        
        self.init(name: name,logo: logo!, address: address!, latitude: latitude, longitude: longitude, phoneNo: phoneNo!, guides: guides!)
    }
    
    init(name: String, logo: UIImage, address: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees, phoneNo: String, guides: [Guide]) {
        super.init()
        self.guides = guides
        self.name = name
        self.logo = logo
        self.address = address
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.phoneNo = phoneNo
    }
    
    //        func getBooking() -> [Booking] {
    //            let booking: [Booking]
    //
    //            return booking
    //        }
    //
    
    func allCategories() -> [Activity] {
        return []
    }
}