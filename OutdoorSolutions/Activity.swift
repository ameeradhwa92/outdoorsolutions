//
//  OutdoorCategory.swift
//  OutdoorSolutions
//
//  Created by Ameer on 19/11/2015.
//  Copyright © 2015 Ameer. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class Activity: NSObject, NSCoding{
    enum ActivityType: String {
        case Hiking = "Hiking"
        case WaterRafting = "Water Rafting"
        case ScubaDiving = "Scuba Diving"
        case Swimming = "Swimming"
        
        func GetCategory() -> String {
            switch self {
            case .Hiking:
                return "Hiking"
            case .ScubaDiving:
                return "Scuba Diving"
            case .Swimming:
                return "Swimming"
            case .WaterRafting:
                return "Water Rafting"
            }
        }
    }
    
    var name: String?
    var address: String?
    var coordinate: CLLocationCoordinate2D?
    var activityDescription: String?
    var image: UIImage?
    var activityType: ActivityType?
    
    func encodeWithCoder(aCoder: NSCoder){
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        keyedArchiver.encodeObject(self.name, forKey: "PLDActivityName")
        keyedArchiver.encodeObject(self.address, forKey: "PLDActivityAddress")
        keyedArchiver.encodeObject(self.coordinate?.latitude, forKey: "PLDActivityLatitude")
        keyedArchiver.encodeObject(self.coordinate?.longitude, forKey: "PLDActivityLongitude")
        keyedArchiver.encodeObject(self.activityDescription, forKey: "PLDActivityDescription")
        
        if self.image != nil {
            let imageData: NSData? = UIImagePNGRepresentation(self.image!)
            keyedArchiver.encodeObject(imageData, forKey: "PLDActivityImageData")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let name: String = keyedUnarchiver.decodeObjectForKey("PLDActivityName") as! String
        let address: String = keyedUnarchiver.decodeObjectForKey("PLDActivityAddress") as! String
        let latitude: CLLocationDegrees = keyedUnarchiver.decodeObjectForKey("PLDActivityLatitude") as! CLLocationDegrees
        let longitude: CLLocationDegrees = keyedUnarchiver.decodeObjectForKey("PLDActivityLongitude") as!
        CLLocationDegrees
        let activityDescription: String = keyedUnarchiver.decodeObjectForKey("PLDActivityDescription") as! String
        
        var image: UIImage? = nil
        if let imageData = keyedUnarchiver.decodeObjectForKey("PLDActivityImageData") as? NSData {
            image = UIImage(data: imageData)
        }
        
        self.init(name: name, address: address, latitude: latitude, longitude: longitude, activityDescription: activityDescription, image: image)
    }

    
    init(name: String, address: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees, activityDescription: String, image: UIImage?) {
        super.init()
        self.name = name
        self.address = address
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.activityDescription = description
        self.image = image
    }
}