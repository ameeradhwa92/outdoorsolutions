//
//  ListGuideViewController.swift
//  OutdoorSolutions
//
//  Created by Siti Shuhada Suharman on 28/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit

class GuideListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var guides: [Guide] = [Guide]()
    var activities: [Activity] = [Activity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        preloadData()
    }

    func preloadData() {
        
        let guide1: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide1)
        let guide2: Guide = Guide(name: "Jason1", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide2)
        let guide3: Guide = Guide(name: "Jason2", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide3)
        let guide4: Guide = Guide(name: "Jason3", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide4)
        let guide5: Guide = Guide(name: "Jason4", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide5)
        
//        for ( var i=0; i < 20; i++ ){
//            let guides: Guide = Guide.createRandomGuide()
//            self.guides.append(guide)
//        }
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GuideListViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guides.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        let guide: Guide = guides[indexPath.row]
        cell.textLabel?.text = guide.name
        
//        cell.textLabel?.text = guides [indexPath.row]
        
        return cell
    }
}

extension GuideListViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        print("User selected row \(indexPath.row)")
        
        performSegueWithIdentifier("segueGuideDetails", sender: self)
    }
}