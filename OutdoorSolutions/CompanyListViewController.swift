//
//  CompanyListViewController.swift
//  OutdoorSolutions
//
//  Created by Ameer on 28/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {
    var company: Company! {
        didSet {
            self.companyNameLabel.text = company.name
            self.companyAddressLabel.text = company.address
            if let coord = company.coordinate {
                self.companyLocationLabel.text = "\(coord.latitude), \(coord.longitude)"
            } else {
                self.companyLocationLabel.text = "Unknown"
            }

            
            self.companyPhoneLabel.text = company.phoneNo
            self.companyImageView.image = company.logo
        }
    }
    
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyAddressLabel: UILabel!
    @IBOutlet weak var companyLocationLabel: UILabel!
    @IBOutlet weak var companyPhoneLabel: UILabel!
    @IBOutlet weak var companyImageView: UIImageView!
}

class CompanyListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var guides: [Guide] = [Guide]()
    var activities: [Activity] = [Activity]()
    var companies: [Company] = [Company]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let activity: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity.activityType = Activity.ActivityType.Hiking
        activities.append(activity)
        
        let guide: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide)
        
        let company = Company(name: "Core Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        let company1 = Company(name: "HikeMe!", logo: UIImage(named: "default")!, address: "Petaling Jaya", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        let company2 = Company(name: "Yipee Hike", logo: UIImage(named: "default")!, address: "Dungun", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        let company3 = Company(name: "Mountain Explorer", logo: UIImage(named: "default")!, address: "Negeri Sembilan", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        let company4 = Company(name: "Adventure Sam", logo: UIImage(named: "default")!, address: "Perak", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        
        companies.append(company)
        companies.append(company1)
        companies.append(company2)
        companies.append(company3)
        companies.append(company4)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: CompanyCell = tableView.dequeueReusableCellWithIdentifier("companyCell", forIndexPath: indexPath) as! CompanyCell
        let company: Company = self.companies[indexPath.row]
        
        cell.company = company
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueCompanyDetails" {
            let vc = segue.destinationViewController as! CompanyDetailsViewController
            
            if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
                let company: Company = self.companies[selectedIndexPath.row]
                vc.company = company
            }
        }
    }

}
