//
//  ActivitiesViewController.swift
//  OutdoorSolutions
//
//  Created by Siti Shuhada Suharman on 25/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit

class ActivitiesViewController: UIViewController {
    @IBOutlet weak var hikingButton: UIButton!
    @IBOutlet weak var scubaButton: UIButton!
    @IBOutlet weak var waterRaftingButton: UIButton!
    
    var centerNavigationController: UINavigationController!
    var centerViewController: ActivityMapViewController!
    
    @IBOutlet weak var swimmingButton: UIButton!
    var activities: [Activity] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func tappedActivity(sender: UIButton) {
        centerViewController = UIStoryboard(name: "Adhwa", bundle: nil).instantiateViewControllerWithIdentifier("ActivityMapViewController") as! ActivityMapViewController
        
        centerNavigationController = UINavigationController(rootViewController: centerViewController)
        view.addSubview(centerNavigationController.view)
        
        if sender == hikingButton {
            
        } else if sender == scubaButton{
            
        } else if sender == waterRaftingButton{
            
        } else if sender == swimmingButton{
            
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
