//
//  GuideDetailsViewController.swift
//  OutdoorSolutions
//
//  Created by Siti Shuhada Suharman on 02/12/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit

class GuideDetailsViewController: UIViewController {
    @IBOutlet weak var guideImage: UIImageView!
    
    @IBOutlet weak var guideNameLabel: UITextField!
    
    @IBOutlet weak var guideExpertise: UITextField!
    @IBOutlet weak var guidePhoneNumber: UITextField!
    
    var centerNavigationController: UINavigationController!
    var centerViewController: BookingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func reservedTapped(sender: AnyObject) {
        centerViewController = UIStoryboard(name: "Adhwa", bundle: nil).instantiateViewControllerWithIdentifier("BookingViewController") as! BookingViewController
        
        centerNavigationController = UINavigationController(rootViewController: centerViewController)
        view.addSubview(centerNavigationController.view)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
