//
//  BookingViewController.swift
//  OutdoorSolutions
//
//  Created by Ameer on 06/12/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit
class BookingCell: UITableViewCell {
    @IBOutlet var rowLabel: UILabel!
    @IBOutlet var rowTextfield: UITextField!
}

class BookingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var name: String = String()
    var phoneNo: String = String()
    var pax: String = String()
    var startDate: String = String()
    var endDate: String = String()
    var formTextfield: [String] = ["Name", "PhoneNo", "Pax", "StartDate", "EndDate"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: BookingCell = tableView.dequeueReusableCellWithIdentifier("bookingCell", forIndexPath: indexPath) as! BookingCell
        
        if indexPath.row == 0 {
            cell.rowLabel.text = "Name"
            formTextfield[0] = cell.rowTextfield.text!
        }
        if indexPath.row == 1 {
            cell.rowLabel.text = "Phone number"
            formTextfield[1] = cell.rowTextfield.text!
        }
        if indexPath.row == 2 {
            cell.rowLabel.text = "Pax"
            formTextfield[2] = cell.rowTextfield.text!
        }
        if indexPath.row == 3 {
            cell.rowLabel.text = "Start date"
            formTextfield[3] = cell.rowTextfield.text!
        }
        if indexPath.row == 4 {
            cell.rowLabel.text = "End date"
            formTextfield[4] = cell.rowTextfield.text!
        }
        if indexPath.row == 5 {
            cell.rowLabel.text = "Book Now!"
            cell.rowLabel.textAlignment = .Center
            cell.rowTextfield.hidden = true
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 5 {
            for index in formTextfield {
                print("\nOutput: \(index)")
            }
            
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
