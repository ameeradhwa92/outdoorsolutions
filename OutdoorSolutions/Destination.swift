//
//  Destination.swift
//  OutdoorSolutions
//
//  Created by Ameer on 21/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class Destination {
    var name: String?
    var address: String?
    var coordinate: CLLocationCoordinate2D?
    var description: String?
    var image: UIImage?
    var activities: [Activity] = []
    
    init(name: String, address: String, latitude: CLLocationDegrees, longitude: CLLocationDegrees, description: String, image: UIImage) {
        self.name = name
        self.address = address
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        self.description = description
        self.image = image
    }
    
}