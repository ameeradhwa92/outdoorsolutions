//
//  ActivityMapViewController.swift
//  OutdoorSolutions
//
//  Created by Ameer on 28/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit
import MapKit

class ActivityMapViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    var centerNavigationController: UINavigationController!
    var centerViewController: ActivitiesViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("activityCell", forIndexPath: indexPath) as UITableViewCell
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Place Name: Gunung Nuang"
        case 1:
            cell.textLabel?.text = "Address: Pahang"
        case 2:
            cell.textLabel?.text = "Coordinate: 3.67357, 103.672637"
        case 3:
            cell.textLabel?.text = "Description: This is the best place to go for hiking."
        default:
            cell.textLabel?.text = "Unavailable"
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    @IBAction func backTapped(sender: AnyObject) {
        centerViewController = UIStoryboard(name: "shue", bundle: nil).instantiateViewControllerWithIdentifier("ActivitiesViewController") as! ActivitiesViewController
        
        self.presentViewController(centerViewController, animated: true, completion: nil)
        
//        centerNavigationController = UINavigationController(rootViewController: centerViewController)
//        view.addSubview(centerNavigationController.view)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
