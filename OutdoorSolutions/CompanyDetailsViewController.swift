//
//  CompanyDetailsViewController.swift
//  OutdoorSolutions
//
//  Created by Ameer on 28/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import UIKit

class CompanyDetailsViewController: UIViewController {
    var company: Company?
    
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyAddress: UILabel!
    @IBOutlet weak var companyPhone: UILabel!
    @IBOutlet weak var companyCoordinate: UILabel!
    @IBOutlet weak var companyLogo: UIImageView!
    
    var centerNavigationController: UINavigationController!
    var centerViewController: GuideListViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        companyName.text = company?.name
        companyAddress.text = company?.address
        companyPhone.text = company?.phoneNo
        companyCoordinate.text = String(company?.coordinate)
        companyLogo.image = company?.logo
        
        let btnName : UIBarButtonItem = UIBarButtonItem(title: "Guide", style: UIBarButtonItemStyle.Plain, target: self, action: "action")
        
        self.navigationItem.rightBarButtonItem = btnName
    }
    
    func action() {
        print("select")
        centerViewController = UIStoryboard(name: "shue", bundle: nil).instantiateViewControllerWithIdentifier("GuideListViewController") as! GuideListViewController
        
        centerNavigationController = UINavigationController(rootViewController: centerViewController)
        view.addSubview(centerNavigationController.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
