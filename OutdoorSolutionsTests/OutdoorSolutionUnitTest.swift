//
//  OutdoorSolutionUnitTest.swift
//  OutdoorSolutions
//
//  Created by Siti Shuhada Suharman on 21/11/2015.
//  Copyright © 2015 Nettsys. All rights reserved.
//

import XCTest
import CoreLocation
@testable import OutdoorSolutions

class OutdoorSolutionUnitTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testLocationInitialization() {
        let location: Activity = Activity(name: "Gunung Nuang", address: "Pahang & Selangor", latitude: 0.0, longitude: 0.0, activityDescription: "Best", image: UIImage(named: "default")!)
        location.activityType = Activity.ActivityType.Hiking
        
        print("name: \(location.name), address: \(location.address), latitude: \(location.coordinate), description: \(location.description)")
        
        XCTAssertNotNil(location, "Location should not be empty")
    }
    
    func testCompanyInitialization() {
        var guides: [Guide] = [Guide]()
        var activities: [Activity] = [Activity]()
        
        let activity: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        
        activity.activityType = Activity.ActivityType.Hiking
        activities.append(activity)
        
        let guide: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide)
        
        let company: Company = Company(name: "Core Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        
        print("name: \(company.name), address: \(company.address), latitude: \(company.coordinate), description: \(company.phoneNo), guide: \(guides.count)")
        
        XCTAssertNotNil(company, "Company should not be empty")
    }
    
    func testListGuide() {
        var guides: [Guide] = [Guide]()
        var activities: [Activity] = [Activity]()
        
        let activity1: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity1.activityType = Activity.ActivityType.Hiking
        activities.append(activity1)
        
        let activity2: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity2.activityType = Activity.ActivityType.ScubaDiving
        activities.append(activity2)
        
        let activity3: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity3.activityType = Activity.ActivityType.Swimming
        activities.append(activity3)
        
        let activity4: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity4.activityType = Activity.ActivityType.WaterRafting
        activities.append(activity4)
        
        let guide1: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide1)
        let guide2: Guide = Guide(name: "Jason1", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide2)
        let guide3: Guide = Guide(name: "Jason2", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide3)
        let guide4: Guide = Guide(name: "Jason3", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide4)
        let guide5: Guide = Guide(name: "Jason4", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide5)
        
        for guide in guides {
            print("name: \(guide.name), expertise: \(guide.expertise), bio: \(guide.bio), phoneNo: \(guide.phoneNo)")
        }
        
        XCTAssertNotNil(guides, "Guide should not be empty")
        XCTAssertNotNil(activities, "Activities should not be empty")
    }
    
    func testBookingInitialize() {
        var activities: [Activity] = [Activity]()
        
        let activity1: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity1.activityType = Activity.ActivityType.Hiking
        activities.append(activity1)
        
        let activity2: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity2.activityType = Activity.ActivityType.ScubaDiving
        activities.append(activity2)
        
        let activity3: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity3.activityType = Activity.ActivityType.Swimming
        activities.append(activity3)
        
        let activity4: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity4.activityType = Activity.ActivityType.WaterRafting
        activities.append(activity4)
        
        let guide1: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        
        let booking: Booking = Booking(userName: "Ahmad Amine", userPhoneNo: "123456789", pax: 9, guide: guide1, categories: guide1.expertise!, startDateTime: NSDate(), endDateTime: NSDate(), confirmationCode: "XCT7653TRS")
        
        print("username: \(booking.userName), phoneNo: \(booking.userPhoneNo), pax: \(booking.pax), guide: \(booking.guide), expertise: \(booking.guide?.expertise), start: \(booking.startDateTime), end: \(booking.endDateTime), code: \(booking.confirmationCode)")
    }
    
    func testSortingCompanyName() {
        var guides: [Guide] = [Guide]()
        var activities: [Activity] = [Activity]()
        var companies: [Company] = [Company]()
        
        let activity1: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity1.activityType = Activity.ActivityType.Hiking
        activities.append(activity1)
        
        let activity2: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity2.activityType = Activity.ActivityType.ScubaDiving
        activities.append(activity2)
        
        let activity3: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity3.activityType = Activity.ActivityType.Swimming
        activities.append(activity3)
        
        let activity4: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity4.activityType = Activity.ActivityType.WaterRafting
        activities.append(activity4)
        
        let guide1: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide1)
        let guide2: Guide = Guide(name: "Jason1", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide2)
        let guide3: Guide = Guide(name: "Jason2", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide3)
        let guide4: Guide = Guide(name: "Jason3", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide4)
        let guide5: Guide = Guide(name: "Jason4", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide5)
        
        let company1: Company = Company(name: "PCore Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company1)
        let company2: Company = Company(name: "Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company2)
        let company3: Company = Company(name: "Core", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company3)
        let company4: Company = Company(name: "LAdventure Core", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company4)
        let company5: Company = Company(name: "HP", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company5)
        
        companies.sortInPlace { (firstCompany: Company, secondCompany: Company) -> Bool in
            return firstCompany.name! < secondCompany.name!
        }
        
        for company in companies {
            print("name: \(company.name), address: \(company.address), latitude: \(company.coordinate), description: \(company.phoneNo), guide: \(guides.count)")
        }
        
        XCTAssertNotNil(companies, "Company should not be empty")
    }
    
    func testSortCoordinateByDistance() {
        var guides: [Guide] = [Guide]()
        var activities: [Activity] = [Activity]()
        var companies: [Company] = [Company]()
        
        let activity1: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity1.activityType = Activity.ActivityType.Hiking
        activities.append(activity1)
        
        let activity2: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity2.activityType = Activity.ActivityType.ScubaDiving
        activities.append(activity2)
        
        let activity3: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity3.activityType = Activity.ActivityType.Swimming
        activities.append(activity3)
        
        let activity4: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity4.activityType = Activity.ActivityType.WaterRafting
        activities.append(activity4)
        
        let guide1: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide1)
        let guide2: Guide = Guide(name: "Jason1", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide2)
        let guide3: Guide = Guide(name: "Jason2", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide3)
        let guide4: Guide = Guide(name: "Jason3", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide4)
        let guide5: Guide = Guide(name: "Jason4", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide5)
        
        let company1: Company = Company(name: "PCore Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 12.34344, longitude: 73.65433, phoneNo: "098456324", guides: guides)
        companies.append(company1)
        let company2: Company = Company(name: "Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company2)
        let company3: Company = Company(name: "Core", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 89.7644, longitude: 123.7643, phoneNo: "098456324", guides: guides)
        companies.append(company3)
        let company4: Company = Company(name: "LAdventure Core", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.7873, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company4)
        let company5: Company = Company(name: "HP", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 2.6674, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company5)
        
        companies.sortInPlace { (firstCompany: Company, secondCompany: Company) -> Bool in
            return firstCompany.name! < secondCompany.name!
        }
        
        let testLocation: CLLocation = CLLocation(latitude: 1.0, longitude: 1.0)
        
        companies.sortInPlace { (co1: Company, co2: Company) -> Bool in
            let location1 = CLLocation(latitude: co1.coordinate!.latitude, longitude: co1.coordinate!.longitude)
            let location2 = CLLocation(latitude: co2.coordinate!.latitude, longitude: co2.coordinate!.longitude)
            
            let dist1: Double = testLocation.distanceFromLocation(location1)
            let dist2: Double = testLocation.distanceFromLocation(location2)
            
            return dist1 < dist2
            // code
        }
        
        
        for company in companies {
            print("name: \(company.name), address: \(company.address), latitude: \(company.coordinate), description: \(company.phoneNo), guide: \(guides.count)")
        }
        
        XCTAssertNotNil(companies, "Company should not be empty")
    }
    
    func testWritingToFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let docDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: .UserDomainMask).first!
        
        let filePath1: NSURL = docDirectory.URLByAppendingPathComponent("activity.plist")
        let filePath2: NSURL = docDirectory.URLByAppendingPathComponent("guide.plist")
        let filePath3: NSURL = docDirectory.URLByAppendingPathComponent("company.plist")
        
        var guides: [Guide] = []
        var activities: [Activity] = []
        var companies: [Company] = []
        
        let activity1: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity1.activityType = Activity.ActivityType.Hiking
        activities.append(activity1)
        
        let activity2: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity2.activityType = Activity.ActivityType.ScubaDiving
        activities.append(activity2)
        
        let activity3: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity3.activityType = Activity.ActivityType.Swimming
        activities.append(activity3)
        
        let activity4: Activity = Activity(name: "Gunung Kinabalu", address: "Sabah", latitude: 103.23212, longitude: 2.87438, activityDescription: "Closed for now", image: UIImage(named: "default")!)
        activity4.activityType = Activity.ActivityType.WaterRafting
        activities.append(activity4)
        
        let guide1: Guide = Guide(name: "Jason", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide1)
        let guide2: Guide = Guide(name: "Jason1", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide2)
        let guide3: Guide = Guide(name: "Jason2", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide3)
        let guide4: Guide = Guide(name: "Jason3", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide4)
        let guide5: Guide = Guide(name: "Jason4", photo: UIImage(named: "default")!, expertise: activities, bio: "Abang sado", phoneNo: "0986545432")
        guides.append(guide5)
        
        let company1: Company = Company(name: "PCore Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 12.34344, longitude: 73.65433, phoneNo: "098456324", guides: guides)
        companies.append(company1)
        let company2: Company = Company(name: "Adventure", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.123, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company2)
        let company3: Company = Company(name: "Core", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 89.7644, longitude: 123.7643, phoneNo: "098456324", guides: guides)
        companies.append(company3)
        let company4: Company = Company(name: "LAdventure Core", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 0.7873, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company4)
        let company5: Company = Company(name: "HP", logo: UIImage(named: "default")!, address: "Shah Alam", latitude: 2.6674, longitude: 1.23, phoneNo: "098456324", guides: guides)
        companies.append(company5)
        
        var activityData: [NSData] = []
        for activity in activities {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(activity)
            activityData.append(data)
        }
        
        var guideData: [NSData] = []
        for guide in guides {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(guide)
            guideData.append(data)
        }
        
        var companyData: [NSData] = []
        for company in companies {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(company)
            companyData.append(data)
        }
        
        (activityData as NSArray).writeToURL(filePath1, atomically: true)
        (guideData as NSArray).writeToURL(filePath2, atomically: true)
        (companyData as NSArray).writeToURL(filePath3, atomically: true)
        
        let isFileExist1: Bool = fileManager.fileExistsAtPath(filePath1.path!)
        XCTAssert(isFileExist1, "File should exist after write")
        NSLog("Filepath: ", filePath1.path!)
        
        let isFileExist2: Bool = fileManager.fileExistsAtPath(filePath2.path!)
        XCTAssert(isFileExist2, "File should exist after write")
        NSLog("Filepath: ", filePath2.path!)
        
        let isFileExist3: Bool = fileManager.fileExistsAtPath(filePath3.path!)
        XCTAssert(isFileExist3, "File should exist after write")
        NSLog("Filepath: ", filePath3.path!)
    }
    
    func testReadFromRead() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let docDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: .UserDomainMask).first!
        let filePath1: NSURL = docDirectory.URLByAppendingPathComponent("activity.plist")
        let filePath2: NSURL = docDirectory.URLByAppendingPathComponent("guide.plist")
        let filePath3: NSURL = docDirectory.URLByAppendingPathComponent("company.plist")
        
        let activityData: [NSData] = NSArray(contentsOfURL: filePath1) as! [NSData]
        var activities: [Activity] = []
        for data in activityData {
            let activity: Activity = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Activity
            activities.append(activity)
        }
        
        let guideData: [NSData] = NSArray(contentsOfURL: filePath2) as! [NSData]
        var guides: [Guide] = []
        for data in guideData {
            let guide: Guide = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Guide
            guides.append(guide)
        }
        
        let companyData: [NSData] = NSArray(contentsOfURL: filePath3) as! [NSData]
        var companies: [Company] = []
        for data in companyData {
            let company: Company = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Company
            companies.append(company)
        }
        
        XCTAssert(activities.count == 4, "Activities should've load 4 from file")
        XCTAssert(guides.count == 5, "Activities should've load 4 from file")
        XCTAssert(companies.count == 5, "Activities should've load 4 from file")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
